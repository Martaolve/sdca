function createXHR() {
	var request = false;
	try {
		request = new ActiveXObject('Msxml2.XMLHTTP');
	} catch (err2) {
		try {
			request = new ActiveXObject('Microsoft.XMLHTTP');
		} catch (err3) {
			try {
				request = new XMLHttpRequest();
			} catch (err1) {
				request = false;
			}
		}
	}
	return request;
}


function get(url, metodo = "GET") {
    // Return a new promise. (resolve be, reject malament)
    return new Promise(function (resolve, reject) {
        // Do the usual XHR stuff
        var req = createXHR();
        //obrir pel metodo tal tal url (ho guarda)
        req.open(metodo, url);
        //on cop carregat fa lo següent
        //onload: quan rebis info... fes aquesta funció
        req.onload = function () {
            // This is called even on 404 etc
            // so check the status
            //estatus 200 executa la resolve
            if (req.status == 200) {
                // Resolve the promise with the response text
                //resolve es una sortida then 
                resolve(req.response);
            }
            else {
                // Otherwise reject with the status text
                // which will hopefully be a meaningful error
                reject(Error(req.statusText));
            }
        };
        //Error del mateix xhr
        // Handle network errors
        req.onerror = function () {
            reject(Error("Network Error"));
        };

        // Make the request. 
        //QUan ho ha fet tot.. envia 
        req.send();
    });
}


function editaYaCreado(url, data, metodo="PATCH") {
    // Return a new promise.
    return new Promise(function (resolve, reject) {
      // Do the usual XHR stuff
      var req = createXHR();
      req.open(metodo, url);
      req.setRequestHeader('Content-type','application/json; charset=utf-8');
  
      req.onload = function () {
        // This is called even on 404 etc
        // so check the status
        if (req.status == 200) {
          // Resolve the promise with the response text
          resolve(req.response);
        }
        else {
          // Otherwise reject with the status text
          // which will hopefully be a meaningful error
          reject(Error(req.statusText));
        }
      };
  
      // Handle network errors
      req.onerror = function () {
        reject(Error("Network Error"));
      };
  
      // Make the request
      req.send(data);
    });
  }

  function creaNuevo (url, data, metodo="POST") {
    // Return a new promise.
    return new Promise(function (resolve, reject) {
      // Do the usual XHR stuff
      var req = createXHR();
      req.open(metodo, url);
      req.setRequestHeader('Content-type','application/json; charset=utf-8');
  
      req.onload = function () {
        // This is called even on 404 etc
        // so check the status
        if (req.status == 200) {
          // Resolve the promise with the response text
          resolve(req.response);
        }
        else {
          // Otherwise reject with the status text
          // which will hopefully be a meaningful error
          reject(Error(req.statusText));
        }
      };
  
      // Handle network errors
      req.onerror = function () {
        reject(Error("Network Error"));
      };
  
      // Make the request
      req.send(data);
    });
  }

  function borrar (url, data, metodo="DELETE") {
    // Return a new promise.
    return new Promise(function (resolve, reject) {
      // Do the usual XHR stuff
      var req = createXHR();
      req.open(metodo, url);
      req.setRequestHeader('Content-type','application/json; charset=utf-8');
  
      req.onload = function () {
        // This is called even on 404 etc
        // so check the status
        if (req.status == 200) {
          // Resolve the promise with the response text
          resolve(req.response);
        }
        else {
          // Otherwise reject with the status text
          // which will hopefully be a meaningful error
          reject(Error(req.statusText));
        }
      };
  
      // Handle network errors
      req.onerror = function () {
        reject(Error("Network Error"));
      };
  
      // Make the request
      req.send(data);
    });
  }