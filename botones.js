function actualizarBD(e) {
    e.preventDefault();
    
    const countryId = document.getElementById("countryId").value;
    const countryName = document.getElementById("countryName").value;
    const countrySpanish = document.getElementById("countrySpanish").value;
    const isoDos = document.getElementById("isoDos").value;

    // var formData = new FormData();

    // formData.append('country_id', countryId);
    // formData.append('short_name', countryName);
    // formData.append('spanish_name', countrySpanish);
    // formData.append('iso2', isoDos);
    // formData.append('numcode', 0);

    let query = "UPDATE cp_pais SET short_name = ?, spanish_name = ?, iso2 = ? WHERE country_id = ?";
    // let query = [countryId, countryName, countrySpanish, isoDos, 0]
    console.log(query)
    const xhr = new XMLHttpRequest();
    xhr.open("http://localhost:3000/api/cp_pais/");
    xhr.send({
        "query": query,
        "params": [countryName, countrySpanish, isoDos, countryId]
    });
    console.log(countryId)


}