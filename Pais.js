
const URL1 = "http://localhost:3000/api/cp_pais/";
const URL = "http://localhost:3000/api/cp_pais/?_p=";

let paginaActual = 0;
let registros2 = "";
let claveNueva = "country_id";
let claveAntigua2 = "country_id";
let alterclave = 1;
let orden = "POST";



//Función para empezar, al arrancar la página. La orden está en el html 
function leedatos() {
    // console.log("leedatos");

    get(URL + paginaActual + "&_size=20").then(function (response) {
        //Al sortir d'aquí tinc la llista muntada. El get està escrit en el comunicaciones (es el new promise)
        //presentalista es el metode per montar la llista
        carregaPaisos(JSON.parse(response));
        console.log("get");
    })
        .catch(function (error) {
            console.error(error);
        });
}
////////////////////////////////////////PAGINACION//////////////////////////////////////////////////////
//Función para coger los datos e iniciar la función para contar el número de filas y poder cargar las páginas
//això és com fer el getJSON
get(URL1 + "count")
    .then(function (response) {
        obtenDatos(JSON.parse(response));
    })
    .catch(function (error) {
        console.error(error);
    });

//función que cuenta el número de filas 
function obtenDatos(respuesta) {
    var registros = respuesta[0].no_of_rows;
    registros2 = registros;
    // console.log(registros);

}
//funciones para pasar pasar páginas o retroceder
function paginaMas() {

    let limite = Math.round(registros2 / 20) - 1;
    // console.log(limite);
    if (paginaActual == limite) {

    } else {
        paginaActual = paginaActual + 1;
        // console.log(paginaActual);
        leedatos();
    }

}

function paginaMenos() {
    if (paginaActual == 0) {

    } else {
        paginaActual = paginaActual - 1;
        // console.log(paginaActual);
        leedatos();
    }
}

///////////////////CARGA DE LA TABLA////////////////////////////////////////////////////////////////////////////////

//coge el array de datos y monta el head 
function carregaPaisos(pais) {
    console.log("carregapaisos");
    // función para montar el tbodie (el thead está en el html)
    carregatbody(pais);
    console.log(orden);
    // console.log("carregapaisos");


}


//monta el tbody
function carregatbody(element) {

    // console.log("carregabody");

    //ordena el arrai que llega segun la opción escogida (en los cabeceros de la tabla)
    let elementOrdenat = element.sort(ordena);


    let seleccionador = document.getElementById('tbody');
    //Vaciado de la tabla previo a la carga nueva
    seleccionador.innerHTML = "";

    //carga de los datos nuevos 
    elementOrdenat.forEach(element => {
        // console.log(element);
        let tr = document.createElement("tr");
        let td0 = document.createElement("td");
        td0.appendChild(document.createTextNode(element.country_id));
        tr.appendChild(td0);
        let td1 = document.createElement("td");
        td1.appendChild(document.createTextNode(element.spanish_name));
        tr.appendChild(td1);
        let td2 = document.createElement("td");
        td2.appendChild(document.createTextNode(element.short_name));
        tr.appendChild(td2);
        let td3 = document.createElement("td");
        td3.appendChild(document.createTextNode(element.iso2));
        tr.appendChild(td3);

        //Botón dentro de la tabla para el formulario editar 
        let td4 = document.createElement("a");
        td4.setAttribute("class", "btn btn-dark");
        td4.setAttribute("id", "buttonEditar");
        td4.setAttribute("onclick", "editar('" + element.country_id + "')");
        td4.appendChild(document.createTextNode("editar"));
        tr.appendChild(td4);

        //Botón dentro de la tabla para suprimir un país 
        let td5 = document.createElement("button");
        td5.appendChild(document.createTextNode("suprimir"));
        td5.setAttribute("class", "btn btn-danger");
        td5.setAttribute("id", "buttonSuprimir");
        td5.setAttribute("onclick", "suprimir('" + element.country_id + "')");
        tr.appendChild(td5);
        seleccionador.appendChild(tr);


    });
}


///////////////////FUNCIONES DE ORDENAR ///////////////////////////


//guarda como ordenarlo (columnas de la tabla)

function guardaClave(clave1) {
    claveAntigua2 = claveNueva;
    claveNueva = clave1;
    console.log("guardaClave");
    leedatos();
    //si la clave es la misma, por lo tanto se ha apretado dos veces seguidas la misma columna, canvia el valor para poder ordenarlo al revés
    if (claveAntigua2 === claveNueva) {
        if (alterclave == 1) {
            // console.log("alterclave: "+alterclave);
            alterclave = 0;
        } else {
            // console.log("alterclave: "+alterclave);
            alterclave = 1;
        }
    }

}

//función de ordenar
function ordena(clave3, clave2) {
    // console.log("ordena");

    // console.log("clave antigua:" + claveAntigua2);
    // console.log("clave Nueva:" + claveNueva);


    //apretando una columna diferente a la anterior
    if (claveAntigua2 !== claveNueva) {
        // console.log("diferents");
        if (clave3[claveNueva] === clave2[claveNueva]) return 0;
        if (clave3[claveNueva] > clave2[claveNueva]) return 1;
        return -1;
    }
    //apretando la misma columna dos veces, para invertir el orden por la misma columna.El alterclave se gestiona en la funcion guardaClave
    if (claveAntigua2 === claveNueva) {
        // console.log("iguals");
        if (alterclave == 1) {
            // console.log("alterclave: "+alterclave);
            if (clave3[claveNueva] === clave2[claveNueva]) return 0;
            if (clave3[claveNueva] > clave2[claveNueva]) return 1;

            return -1;

        }
        if (alterclave == 0) {
            // console.log("alterclave: "+alterclave);
            if (clave3[claveNueva] === clave2[claveNueva]) return 0;
            if (clave3[claveNueva] > clave2[claveNueva]) return -1;

            return 1;



        }
    }
}


///////////////FUNCIONES PARA SUPRIMIR PAIS////////////////

function suprimir(id) {
    console.log("suprimir");
    //confirm es un cuadro que aparece con dos botones. El aceptar es true y el cancelar false . El cancelar aquí solo cierra el cuadro del confirm
    let confirm2 = confirm("Seguro que quieres eleminarlo?");

    if (confirm2 == true) {
        supimirDefinitivamente(id);
        console.log("holi");
    }
}


//una vez apretado el botón aceptar del confirm, función para suprimir el país
function supimirDefinitivamente(id) {

    console.log("suprimir definitivamente");
    //se cogen los datos de la base de datos para montar el objeto para ese país
    let id0 = id[0];
    let id2 = id0.country_id;
    let iso22 = id0.iso2;
    let sn = id0.short_name;
    let spn = id0.spanish_name;

    //se crea el objeto
    let datos = {
        country_id: id2,
        iso2: iso22,
        short_name: sn,
        spanish_name: spn,
        calling_code: " ",
        cctld: " ",
        iso3: " ",
        long_name: " ",
        numcode: 0,
        un_member: " ",

    }

    let json = JSON.stringify(datos);
    console.log(datos);
    //función de borrar que está en comunicaciones
    borrar(URL1 + id, json, orden = "DELETE")
        .then((response) => {
            console.log(response);


        })
        .catch((error) => {
            console.log("Error");
        });
    location.reload();

}

////////////////////////////////FUNCIONES PARA EDITAR UN PAIS/////////////////
//un cop apretat el botó edita 
function editar(id) {
    console.log("edita");
    orden = "PATCH";
    console.log(orden);



    //crida al get 
    get(URL1 + id).then(function (response) {
        //Al sortir d'aquí tinc la llista muntada. El get està escrit en el comunicaciones (es el new promise)
        //presentalista es el metode per montar la llista
        carregaEdita(JSON.parse(response));
        console.log("get");
    })
        .catch(function (error) {
            console.error(error);
        });




}
//carrega el formulario edita 
function carregaEdita(id) {
    amagaMostra();
    let tituloEdita = document.getElementById('tituloform');
    let h1 = document.createElement('h1');
    h1.appendChild(document.createTextNode('Edita el país'));
    tituloEdita.appendChild(h1);

    let id0 = id[0];

    let country_id = document.getElementById('countryIdInsertar');
    country_id.value = id0.country_id;
    country_id.setAttribute("readonly", "readonly");
    let countrySpanish = document.getElementById('countrySpanish');
    countrySpanish.value = id0.spanish_name;
    let shortName = document.getElementById('shortName');
    shortName.value = id0.short_name;
    let isoDos = document.getElementById('isoDos');
    isoDos.value = id0.iso2;


}

//enviar el formulario (despres del botó enviar). Sirve tanto para país nuevo como para edita (Incorpora el país en la tabla)
function enviar() {

    //recoge los datos del formulario
    let id = document.getElementById('countryIdInsertar').value;
    let countrySpanish = document.getElementById('countrySpanish').value;
    let shortName = document.getElementById('shortName').value;
    let isoDos = document.getElementById('isoDos').value;
    console.log(countrySpanish);
    // console.log(id);
    console.log(shortName);
    console.log(isoDos);


    //Crea un objeto con los datos 
    let datos = {
        country_id: id,
        iso2: isoDos,
        short_name: shortName,
        spanish_name: countrySpanish,
        calling_code: " ",
        cctld: " ",
        iso3: " ",
        long_name: " ",
        numcode: 0,
        un_member: " ",


    }

    let json = JSON.stringify(datos);
///para editar país 
    if (orden === "PATCH") {
        editaYaCreado(URL1 + id, json, orden = "PATCH")
            .then((response) => {
                console.log(response);

                orden = "POST";
            })
            .catch((error) => {
                console.log("Error");
            });


    } 
    //para crear un país nuevo
    else if (orden === "POST") {
        creaNuevo(URL1, json, orden = "POST")
            .then((response) => {
                console.log(response);

            })
            .catch((error) => {
                console.log("Error");
            });
    }
}


//////////////////////////FUNCIÓN PARA CREAR UN NUEVO PAÍS //////////////
//(Al ser el mismo formulario que editar el botón enviar está vinculado a la misma función que editar. Función enviar)
function nuevoPais() {
    amagaMostra();
    let tituloEdita = document.getElementById('tituloform');
    let h1 = document.createElement('h1');
    h1.appendChild(document.createTextNode('Crea un nuevo país'));
    tituloEdita.appendChild(h1);
    console.log("344" + orden);

}



////PARA ESCONDER LA TABLA Y MOSTRAR EL FORMULARIO, una vez apretado el botón editar o nuevo país
function amagaMostra() {
    let titulo = document.getElementById('titulo');
    titulo.classList.add("amaga");
    let tabla = document.getElementById('tabla');
    tabla.classList.add("amaga");
    let flechas = document.getElementById('flechas');
    flechas.classList.add("amaga");
    let formulario = document.getElementById('formulario');
    formulario.classList.remove("amaga");
}

